package database

import (
	"crypto/tls"
	"fmt"
	"github.com/globalsign/mgo"
	"github.com/sirupsen/logrus"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"net"
	"net/http"
)

func CosmosDBConnect(url string) *mgo.Session {

	if len(url) == 0 {
		panic("Can`t start because cosmos db uri is empty")
	}

	dialInfo, err := mgo.ParseURL(url)

	if err != nil {
		e := fxerror.NewError("internal", fxerror.Internal, "Conexão com banco de dados", "", http.StatusInternalServerError, "", err)
		panic(e)
	}

	tlsConfig := tls.Config{}

	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), &tlsConfig)
		return conn, err
	}
	session, err := mgo.DialWithInfo(dialInfo)

	if err != nil {
		e := fxerror.NewError("internal", fxerror.Internal, "Conexão com banco de dados", "", http.StatusInternalServerError, "", err)
		panic(e)
	} else {
		logrus.Info(fmt.Sprintf("Connected with CosmosDB in database:[%s]", dialInfo.Database))
	}

	session.SetMode(mgo.Monotonic, true)

	logrus.Info()

	return session
}
