package fxerror

import (
	"bytes"
	"fmt"
)

// Application fxerror codes.
const (
	AuthDeclined             = "Cobrança recusada."
	CaptureUnequalInstrument = "Não é possível capturar um valor diferente do que o autorizado."
	CaptureVoided            = "Não é possível capturar uma cobrança cancelada."
	PartialCaptureInstrument = "Não é possível capturar parcialmente a cobrança."
	CaptureDeclined          = "Captura da cobrança negada."
	CaptureLimitExceeded     = "Excedido o valor máximo de captura para a cobrança."
	ExpiredAuthorization     = "Não é possível capturar cobrança com a autorização expirada."
	InvalidField             = "Um campo inválido resultou em uma requisição inválida."
	NotFound                 = "Não foi possível encontrar o recurso definido na requisição."
	InvalidCredentials       = "Credenciais inválidas."
	InvalidRequest           = "Input inválido."
	Unauthorized             = "Credenciais inválidas."
	Internal                 = "Erro interno do servidor."
	Duplicate                = "Não foi possível criar objeto já existente."
)

type TaxDocument struct {
	Type   string `json:"-"` // "CPF"
	Number string `json:"number,omitempty"`
}

type Customer struct {
	TaxDocument TaxDocument `json:"taxDocument,omitempty"`
}

type Error struct {

	// Campo com erro, pode ser nulo
	Field string `json:"field,omitempty"`

	// Uma mensagem descrevendo o erro
	Message string `json:"message,omitempty"`

	// Um código curto de referência para um erro
	Code string `json:"code,omitempty"`

	// Tipo de erro retornado, pode ser nulo
	ErrType string `json:"type,omitempty"`

	// A mensagem do status retornado
	Status string `json:"status,omitempty"`

	// O status HTTP retornado
	StatusCode int `json:"statusCode,omitempty"`

	// Número do documento do customer, pode ser nulo
	Customer *Customer `json:"customer,omitempty"`

	Err error `json:"error,omitempty"`
}

// Error returns the string representation of the fxerror message.
func (e *Error) Error() string {
	var buf bytes.Buffer

	// If wrapping an fxerror, print its Error() message.
	// Otherwise print the fxerror code & message.
	if e.Err != nil {
		buf.WriteString(e.Err.Error())
	} else {
		if e.Code != "" {
			fmt.Fprintf(&buf, "<%s> ", e.Code)
		}
		buf.WriteString(e.Message)
	}
	return buf.String()
}

// ErrorCode returns the code of the root fxerror, if available. Otherwise returns Internal.
func ErrorCode(err error) string {
	if err == nil {
		return ""
	} else if e, ok := err.(*Error); ok && e.Code != "" {
		return e.Code
	} else if ok && e.Err != nil {
		return ErrorCode(e.Err)
	}
	return Internal
}

// ErrorMessage returns the human-readable message of the fxerror, if available.
// Otherwise returns a generic fxerror message.
func ErrorMessage(err error) string {
	if err == nil {
		return ""
	} else if e, ok := err.(*Error); ok && e.Message != "" {
		return e.Message
	} else if ok && e.Err != nil {
		return ErrorMessage(e.Err)
	}
	return "Erro interno do servidor."
}

func NewError(code, message, field, documentNumber string, statusCode int, errType string, err error) *Error {

	taxDocument := TaxDocument{
		Number: documentNumber,
	}

	customer := &Customer{
		TaxDocument: taxDocument,
	}

	if documentNumber == "" {
		customer = nil
	}

	var stringStatusCode string

	switch statusCode {
	case 400:
		stringStatusCode = "Campos requisitados geraram um erro."
	case 401:
		stringStatusCode = "Não autorizado."
	case 404:
		stringStatusCode = "Não é possível encontrar o recurso."
	case 500:
		stringStatusCode = "Erro interno do servidor."
	default:
		stringStatusCode = "Erro interno do servidor."
	}

	return &Error{
		Field:      field,
		Message:    message,
		Code:       code,
		ErrType:    errType,
		Status:     stringStatusCode,
		StatusCode: statusCode,
		Customer:   customer,
		Err:        err,
	}
}

type CustomError struct {
	Message string `json:"message"`
}

func NewCustomError(message string) CustomError {
	return CustomError{Message: message}
}

func (e CustomError) Error() string {
	return e.Message
}
