module bitbucket.org/guilhermesilvafluxx/core

require (
	github.com/Nhanderu/brdoc v1.1.2
	github.com/asaskevich/govalidator v0.0.0-20190424111038-f61b66f89f4a // indirect
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-ozzo/ozzo-validation/v4 v4.1.0
	github.com/logrusorgru/aurora v0.0.0-20190803045625-94edacc10f9b
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/nouney/randomstring v0.0.0-20180330205616-1374daa59f01
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.4.0
	go.mongodb.org/mongo-driver v1.3.3
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
	golang.org/x/text v0.3.2
)

go 1.13
