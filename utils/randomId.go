package utils

import (
	"fmt"
	"github.com/nouney/randomstring"
	"strings"
)

func GenerateRandomId(numLetters, numDigits int, prefix, separator string) (string, error) {
	var builder strings.Builder

	_, _ = fmt.Fprintf(&builder, "%s", prefix)

	rsg, err := randomstring.NewGenerator(randomstring.CharsetAlphaUp)
	if err != nil {
		return "", err
	}
	_, _ = fmt.Fprintf(&builder, "%s", rsg.Generate(numLetters))

	_, _ = fmt.Fprintf(&builder, "%s", separator)

	rsg, err = randomstring.NewGenerator(randomstring.CharsetNum)
	if err != nil {
		return "", err
	}
	_, _ = fmt.Fprintf(&builder, "%s", rsg.Generate(numDigits))

	return builder.String(), nil
}

func GenerateRandomTransactionId() (string, error) {
	rsg, err := randomstring.NewGenerator(randomstring.CharsetAlphaUp, randomstring.CharsetAlphaLow, randomstring.CharsetNum)
	if err != nil {
		return "", err
	}

	random := rsg.Generate(16)

	return random, nil
}
