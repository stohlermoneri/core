package utils

import "strings"

func DashToSlashDate(date string) string {
	split := strings.Split(date, "-")
	return split[2] + "/" + split[1] + "/" + split[0]
}

func SlashToDashDate(date string) string {
	split := strings.Split(date, "/")
	return split[2] + "-" + split[1] + "-" + split[0]
}
