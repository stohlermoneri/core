package utils

import (
	"encoding/json"
	"fmt"
	. "github.com/logrusorgru/aurora"
	"io"
	"time"
)

func now() string {
	return time.Now().Format("2006-01-02 15:04:05") + " - "
}

func LogInfo(message string) {
	fmt.Println(Blue(now() + message))
}

func LogError(message string) {
	fmt.Println(Red(now() + message))
}

func LogSuccess(message string) {
	fmt.Println(Green(now() + message))
}

func LogJSON(message string, str interface{}) {
	b, err := json.Marshal(str)
	if err != nil {
		LogError(err.Error())
	} else {
		fmt.Println(Blue(now() + message + string(b)))
	}
}

func LogBody(resp io.ReadCloser) error {
	var body map[string]interface{}
	if err := json.NewDecoder(resp).Decode(&body); err != nil {
		return err
	}

	if len(body) != 0 {
		fmt.Print(now())
		fmt.Println(Blue(body))
	}

	return nil
}
