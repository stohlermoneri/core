package utils

import (
	"strings"
	"time"
)

func StringsToPathParams(params ...string) string {
	var sb strings.Builder
	for _, str := range params {
		sb.WriteString("/")
		sb.WriteString(str)
	}
	joined := sb.String()

	return joined
}

func DateToPeriod(date time.Time, months int64) (string, string) {
	// Returns date and finalDate, which is date + numberOfInstallments (months)

	finalDate := date.AddDate(0, int(months), 0)

	return date.Format("02/01/2006"), finalDate.Format("02/01/2006")
}
