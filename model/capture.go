package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"net/http"
	"time"
)

type CaptureRequest struct {
	OwnId                string   `json:"ownId,omitempty" bson:"ownId,omitempty"`
	ShippingConfirmation string   `json:"shipping_confirmation,omitempty" bson:"shipping_confirmation,omitempty"`
	ShippingCarrier      string   `json:"shipping_carrier,omitempty" bson:"shipping_carrier,omitempty"`
	Customer             Customer `json:"customer,omitempty" bson:"customer,omitempty"`
	MerchantName         string   `json:"-" bson:"merchantName,omitempty"`
}

type CaptureResponse struct { // Pode ser captura de cobrança autorizada ou anulação
	Created              time.Time `json:"created,omitempty" bson:"created,omitempty"`
	OwnId                string    `json:"ownId,omitempty" bson:"ownId,omitempty"`
	ShippingConfirmation string    `json:"shipping_confirmation,omitempty" bson:"shipping_confirmation,omitempty"`
	ShippingCarrier      string    `json:"shipping_carrier,omitempty" bson:"shipping_carrier,omitempty"`
	ShippingAddress      Address   `json:"shippingAddress,omitempty" bson:"shippingAddress,omitempty"`
	Currency             string    `json:"currency,omitempty" bson:"currency,omitempty"`
	Amount               int32     `json:"amount,omitempty" bson:"amount,omitempty"`
	Type                 string    `json:"capture,omitempty" bson:"capture,omitempty"`
	Id                   string    `json:"id,omitempty" bson:"id,omitempty"`
	TransactionId        string    `json:"transaction_id,omitempty" bson:"transaction_id,omitempty"`
}

func (c CaptureRequest) Validate() error {
	err := validation.Errors{
		"charge_id": validation.Validate(&c.OwnId, validation.Required, validation.Length(1, 45)),
		"ownId":     validation.Validate(&c.OwnId, validation.Required, validation.Length(1, 45)),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}
