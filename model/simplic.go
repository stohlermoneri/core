package model

type SimplicOffer struct {
	NumInstallments   string `json:"num_installments,omitempty" bson:"num_installments,omitempty"`
	InterestRate      string `json:"interest_rate,omitempty" bson:"interest_rate,omitempty"`
	InstallmentAmount string `json:"installment_amount,omitempty" bson:"installment_amount,omitempty"`
	CetRate           string `json:"cet_rate,omitempty" bson:"cet_rate,omitempty"`
	Amount            string `json:"amount,omitempty" bson:"amount,omitempty"`
}

type SimplicLoan struct {
	ProviderReference string         `json:"provider_reference" bson:"provider_reference,omitempty"`
	Decision          string         `json:"decision" bson:"decision,omitempty"`
	Offers            []SimplicOffer `json:"offers" bson:"offers,omitempty"`
	ConfirmationURL   string         `json:"confirmation_url" bson:"confirmation_url,omitempty"`
	Errors            string         `json:"errors" bson:"errors,omitempty"`
}
