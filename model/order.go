package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"regexp"
	"time"
)

type Order struct {
	Id           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	OrderId      string             `json:"orderId,omitempty" bson:"orderId,omitempty"`
	WirecardId   string             `json:"wirecardId,omitempty" bson:"wirecardId,omitempty"`
	OwnId        string             `json:"ownId,omitempty" bson:"ownId,omitempty"`
	Amount       Amount             `json:"amount,omitempty" bson:"amount,omitempty"`
	Items        []Item             `json:"items,omitempty" bson:"items,omitempty"`
	Customer     Customer           `json:"customer,omitempty" bson:"customer,omitempty"`
	Merchant     Merchant           `json:"merchant,omitempty" bson:"merchant,omitempty"`
	RedirectURL  string             `json:"redirectUrl,omitempty" bson:"redirectUrl,omitempty"`
	Status       string             `json:"status,omitempty" bson:"status,omitempty"`
	CreatedAt    time.Time          `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	UpdatedAt    time.Time          `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
	MerchantName string             `json:"-" bson:"merchantName,omitempty"`
}

func (o Order) Validate() error {
	err := validation.Errors{
		//"ownId":                           validation.Validate(&o.OwnId, validation.Required, validation.Length(1, 45)),
		"amount": validation.Validate(&o.Amount),
		//"items":                           validation.Validate(&o.Items, validation.Required),
		"customer": validation.Validate(&o.Customer, validation.Required),
		//"merchant.confirmationUrl":        validation.Validate(&o.Merchant.ConfirmationURL, validation.Required, is.URL),
		//"merchant.userConfirmationAction": validation.Validate(&o.Merchant.UserConfirmationAction, validation.In("POST", "GET")),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}

func (a Amount) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Total, validation.Required.Error("field required")),
		validation.Field(&a.Subtotal),
	)
}

func (a Subtotal) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Shipping, validation.Max(999999999)),
		validation.Field(&a.Discount, validation.Max(999999999)),
		validation.Field(&a.Addition, validation.Max(999999999)),
	)
}

func (i Item) Validate() error {
	err := validation.Errors{
		"product":  validation.Validate(&i.Product, validation.Required, validation.Length(1, 250)),
		"category": validation.Validate(&i.Category, validation.Length(1, 256)),
		"detail":   validation.Validate(&i.Detail),
		"quantity": validation.Validate(&i.Quantity, validation.Required, validation.Max(99999)),
		"price":    validation.Validate(&i.Price, validation.Required, validation.Max(99999999)),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}

func (a Address) Validate() error {
	err := validation.Errors{
		"state":   validation.Validate(&a.State, validation.Length(1, 2)),
		"country": validation.Validate(&a.Country, validation.When(len(a.Country) == 2, is.CountryCode2), validation.When(len(a.Country) == 3, is.CountryCode3)),
		"zipCode": validation.Validate(&a.ZipCode, validation.Length(1, 20)),
		"type":    validation.Validate(&a.Type, validation.Length(1, 20)),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}

func (c Customer) Validate() error {
	err := validation.Errors{
		"customer.email":       validation.Validate(&c.Email, validation.Length(1, 90)),
		"customer.birthDate":   validation.Validate(&c.BirthDate, validation.Match(regexp.MustCompile(`([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))`))),
		"customer.taxDocument": validation.Validate(&c.TaxDocument),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}
func (t TaxDocument) Validate() error {
	err := validation.Errors{
		"type": validation.Validate(&t.Type, validation.In("CNPJ", "CPF", "RG")),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}

type Subtotal struct {
	Shipping int32 `json:"shipping,omitempty" bson:"shipping"`
	Discount int32 `json:"discount,omitempty" bson:"discount"`
	Addition int32 `json:"addition,omitempty" bson:"addition"`
}

type Amount struct {
	Total    int32    `json:"total,omitempty" bson:"total,omitempty"`
	Subtotal Subtotal `json:"subtotal,omitempty" bson:"subtotal,omitempty"`
}

type Item struct {
	Product  string `json:"product,omitempty" bson:"product,omitempty"`
	Category string `json:"category,omitempty" bson:"category,omitempty"`
	Detail   string `json:"detail,omitempty" bson:"detail,omitempty"`
	Quantity int32  `json:"quantity,omitempty" bson:"quantity,omitempty"`
	Price    int32  `json:"price,omitempty" bson:"price,omitempty"`
}

type Address struct {
	Street       string `json:"street,omitempty" bson:"street,omitempty"`
	StreetNumber string `json:"streetNumber,omitempty" bson:"streetNumber,omitempty"`
	Complement   string `json:"complement,omitempty" bson:"complement,omitempty"`
	District     string `json:"district,omitempty" bson:"district,omitempty"`
	City         string `json:"city,omitempty" bson:"city,omitempty"`
	State        string `json:"state,omitempty" bson:"state,omitempty"`
	Country      string `json:"country,omitempty" bson:"country,omitempty"` // "BRA"
	ZipCode      string `json:"zipCode,omitempty" bson:"zipCode,omitempty"`
	Type         string `json:"type,omitempty" bson:"type,omitempty"`
}

type Phone struct {
	CountryCode int32 `json:"countryCode,omitempty" bson:"countryCode,omitempty"`
	AreaCode    int32 `json:"areaCode,omitempty" bson:"areaCode,omitempty"`
	Number      int32 `json:"number,omitempty" bson:"number,omitempty"`
}

type CellPhone struct {
	CountryCode int32 `json:"countryCode,omitempty" bson:"countryCode,omitempty"`
	AreaCode    int32 `json:"areaCode,omitempty" bson:"areaCode,omitempty"`
	Number      int32 `json:"number,omitempty" bson:"number,omitempty"`
}

type TaxDocument struct {
	Type   string `json:"type,omitempty" bson:"type,omitempty"` // "CPF, CNPJ, RG"
	Number string `json:"number,omitempty" bson:"number,omitempty"`
}

type Customer struct {
	OwnId           string      `json:"ownId,omitempty" bson:"ownId,omitempty"`
	FullName        string      `json:"fullName,omitempty" bson:"fullName,omitempty"`
	Email           string      `json:"email,omitempty" bson:"email,omitempty"`
	BirthDate       string      `json:"birthDate,omitempty" bson:"birthDate,omitempty"` // "1990-10-22"
	TaxDocument     TaxDocument `json:"taxDocument,omitempty" bson:"taxDocument,omitempty"`
	Phone           Phone       `json:"phone,omitempty" bson:"phone,omitempty"`
	Mobile          Phone       `json:"mobile,omitempty" bson:"mobile,omitempty"`
	Addresses       []Address   `json:"addresses,omitempty" bson:"addresses,omitempty"`
	ShippingAddress Address     `json:"shippingAddress,omitempty" bson:"shippingAddress,omitempty"`
}

type Merchant struct {
	Id                     string `json:"id,omitempty" bson:"id,omitempty"`
	ConfirmationURL        string `json:"confirmationUrl,omitempty" bson:"confirmationUrl,omitempty"`
	CancelURL              string `json:"cancelUrl,omitempty" bson:"cancelUrl,omitempty"`
	UserConfirmationAction string `json:"userConfirmationAction,omitempty" bson:"userConfirmationAction,omitempty"` // "POST" or "GET"
	Name                   string `json:"name,omitempty" bson:"name,omitempty"`
}

func (m Merchant) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.ConfirmationURL, validation.Required.Error("field required"), is.URL.Error("must be a valid url")),
		validation.Field(&m.CancelURL, validation.Required.Error("field required"), is.URL.Error("must be a valid url")),
		validation.Field(&m.UserConfirmationAction, validation.Required.Error("field required"), validation.In("POST", "GET").Error("must be 'POST' or 'GET'")),
	)
}
