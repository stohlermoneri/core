package model

type MerchantInfo struct {
	MerchantId      string `json:"merchantId,omitempty"`
	MerchantName    string `json:"merchantName,omitempty"`
	BackgroundImage string `json:"backgroundImageUrl,omitempty"`
	LogoUrl         string `json:"logoUrl,omitempty"`
}
