package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"net/http"
)

type Fluxx struct {
	Id        string `json:"id,omitempty" bson:"id,omitempty"`
	Amount    int32  `json:"amount,omitempty" bson:"amount,omitempty"`
	AuthHold  int32  `json:"auth_hold,omitempty" bson:"auth_hold,omitempty"`
	ChargeURL string `json:"charge_url,omitempty" bson:"charge_url,omitempty"`
}

type FluxxOrder struct {
	WirecardPayId string `json:"wirecardPayId,omitempty" bson:"wirecardPayId,omitempty"`
	OwnId         string `json:"ownId,omitempty" bson:"ownId,omitempty"`
	Status        string `json:"status,omitempty" bson:"status,omitempty"`
	Amount        Amount `json:"amount,omitempty" bson:"amount,omitempty"`
	Fluxx         Fluxx  `json:"fluxx,omitempty" bson:"fluxx,omitempty"`
}

func (f FluxxOrder) Validate() error {
	err := validation.Errors{
		"wirecardPayId":    validation.Validate(&f.WirecardPayId, validation.Required, validation.Length(1, 45)),
		"ownId":            validation.Validate(&f.OwnId, validation.Required, validation.Length(1, 45)),
		"status":           validation.Validate(&f.Status, validation.Required, validation.In("CANCELLED")),
		"amount":           validation.Validate(&f.Amount, validation.Required),
		"amount.total":     validation.Validate(&f.Amount.Total, validation.Required),
		"fluxx":            validation.Validate(&f.Fluxx, validation.Required),
		"fluxx.id":         validation.Validate(&f.Fluxx.Id, validation.Required, validation.Length(1, 45)),
		"fluxx.amount":     validation.Validate(&f.Fluxx.Amount, validation.Required),
		"fluxx.auth_hold":  validation.Validate(&f.Fluxx.AuthHold, validation.Required),
		"fluxx.charge_url": validation.Validate(&f.Fluxx.ChargeURL, validation.Required),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}
