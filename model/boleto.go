package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"net/http"
)

type BoletoDetails struct {
	LineCode       string `json:"lineCode,omitempty" bson:"lineCode,omitempty"`
	ExpirationDate string `json:"expirationDate,omitempty" bson:"expirationDate,omitempty"`
}

type Boleto struct {
	WirecardId string        `json:"wirecardId,omitempty" bson:"wirecardId,omitempty"`
	OwnId      string        `json:"ownId,omitempty" bson:"ownId,omitempty"`
	Boleto     BoletoDetails `json:"boleto,omitempty" bson:"boleto,omitempty"`
}

func (b Boleto) Validate() error {
	err := validation.Errors{
		"wirecardId":            validation.Validate(&b.WirecardId, validation.Required, validation.Length(1, 45)),
		"ownId":                 validation.Validate(&b.OwnId, validation.Required, validation.Length(1, 45)),
		"boleto":                validation.Validate(&b.Boleto, validation.Required),
		"boleto.lineCode":       validation.Validate(&b.Boleto.LineCode, validation.Required),
		"boleto.expirationDate": validation.Validate(&b.Boleto.ExpirationDate, validation.Required),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}
