package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"net/http"
	"regexp"
)

type Value struct {
	Term int64      `json:"term,omitempty" bson:"term,omitempty"`
	Min  float64    `json:"min,omitempty" bson:"min,omitempty"`
	Max  float64    `json:"max,omitempty" bson:"max,omitempty"`
	Tax  [2]float64 `json:"tax,omitempty" bson:"tax,omitempty"`
}

type Offer struct {
	Tag              string  `json:"tag,omitempty" bson:"tag,omitempty"`
	InstalmentValue  float64 `json:"instalment_value,omitempty" bson:"instalment_value,omitempty"`
	InterestRate     float64 `json:"interest,omitempty" bson:"interest,omitempty"`
	InstalmentNumber int64   `json:"instalment_number,omitempty" bson:"instalment_number,omitempty"`
	Principal        float64 `json:"principal,omitempty" bson:"principal,omitempty"`
	URL              string  `json:"suggested_offer_url,omitempty" bson:"suggested_offer_url,omitempty"`
}

type Result struct {
	URL            string  `json:"url,omitempty" bson:"url,omitempty"`
	Approval       bool    `json:"approval,omitempty" bson:"approval,omitempty"`
	Values         []Value `json:"values,omitempty" bson:"values,omitempty"`
	Offers         []Offer `json:"offers,omitempty" bson:"offers,omitempty"`
	SuggestedOffer Offer   `json:"suggested_offer,omitempty" bson:"suggested_offer,omitempty"`
}

type GeruOffer struct {
	UUID   string `json:"uuid,omitempty" bson:"uuid,omitempty"`
	Status string `json:"status,omitempty" bson:"status,omitempty"`
	URL    string `json:"url,omitempty" bson:"url,omitempty"`
	Result Result `json:"result,omitempty" bson:"result,omitempty"`
}

type GeruLoan struct {
	LoanUUID string `json:"loan_uuid,omitempty" bson:"loan_uuid,omitempty"`
	Status   string `json:"status,omitempty" bson:"status,omitempty"`
	URL      string `json:"url,omitempty" bson:"url,omitempty"`
}

type LoanRequest struct {
	Email         string `json:"email,omitempty" bson:"email,omitempty"`
	CPF           string `json:"cpf,omitempty" bson:"cpf,omitempty"` // 778.315.400-08
	Password      string `json:"password,omitempty" bson:"password,omitempty"`
	Name          string `json:"name,omitempty" bson:"name,omitempty"`
	Birthdate     string `json:"birthdate,omitempty" bson:"birthdate,omitempty"` // 1979-11-11
	Gender        string `json:"gender,omitempty" bson:"gender,omitempty"`
	RG            string `json:"rg,omitempty" bson:"rg,omitempty"`
	Origin        string `json:"rg_orgao_expedidor,omitempty" bson:"rg_orgao_expedidor,omitempty"`
	EmissionDate  string `json:"rg_date_of_emission,omitempty" bson:"rg_date_of_emission,omitempty"` // 1996-01-01
	EmissionState string `json:"uf_orgao_expedidor,omitempty" bson:"uf_orgao_expedidor,omitempty"`
	Cellphone     string `json:"cellphone_number,omitempty" bson:"cellphone_number,omitempty"` // 11987459877
	Phone         string `json:"phone_number,omitempty" bson:"phone_number,omitempty"`         // 11987459877
	MotherName    string `json:"mother_name,omitempty" bson:"mother_name,omitempty"`
	MaritalState  string `json:"marital_state,omitempty" bson:"marital_state,omitempty"`
	Naturality    string `json:"naturality,omitempty" bson:"naturality,omitempty"`

	Scholarity string `json:"scholarity,omitempty" bson:"scholarity,omitempty"`
	Employment string `json:"employment,omitempty" bson:"employment,omitempty"`
	Company    string `json:"employment_company,omitempty" bson:"employment_company,omitempty"`
	JobTitle   string `json:"employment_post,omitempty" bson:"employment_post,omitempty"`
	Salary     string `json:"employment_salary,omitempty" bson:"employment_salary,omitempty"` // 11000.00
	Occupation string `json:"employment_status,omitempty" bson:"employment_status,omitempty"`

	Address      string `json:"address_street,omitempty" bson:"address_street,omitempty"`
	Number       string `json:"address_number,omitempty" bson:"address_number,omitempty"`
	Complement   string `json:"address_complement,omitempty" bson:"address_complement,omitempty"`
	Neighborhood string `json:"address_neighborhood,omitempty" bson:"address_neighborhood,omitempty"`
	City         string `json:"address_city,omitempty" bson:"address_city,omitempty"`
	State        string `json:"address_state,omitempty" bson:"address_state,omitempty"`
	ZipCode      string `json:"address_zip_code,omitempty" bson:"address_zip_code,omitempty"` // No -
	ResidentType string `json:"address_residence_type,omitempty" bson:"address_residence_type,omitempty"`

	BankCode             string `json:"account_bank_code,omitempty" bson:"account_bank_code,omitempty"`
	BranchNumber         string `json:"account_branch_number,omitempty" bson:"account_branch_number,omitempty"` // No -
	AccountNumber        string `json:"account_number,omitempty" bson:"account_number,omitempty"`               // No -
	LoanPrincipal        string `json:"loan_principal,omitempty" bson:"loan_principal,omitempty"`               // 11000.00
	LoanInstalmentNumber string `json:"loan_instalment_number,omitempty" bson:"loan_instalment_number,omitempty"`
	LoanReason           string `json:"loan_reason,omitempty" bson:"loan_reason,omitempty"`
}

func (l LoanRequest) Validate() error {
	err := validation.Errors{
		"email":               validation.Validate(&l.Email, validation.Required),
		"cpf":                 validation.Validate(&l.CPF, validation.Required, validation.Match(regexp.MustCompile(`\d{3}.\d{3}.\d{3}-\d{2}`))),
		"password":            validation.Validate(&l.Password, validation.Required),
		"name":                validation.Validate(&l.Name, validation.Required),
		"birthdate":           validation.Validate(&l.Birthdate, validation.Required, validation.Match(regexp.MustCompile(`([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))`))),
		"gender":              validation.Validate(&l.Gender, validation.Required, validation.In("M", "F")),
		"rg":                  validation.Validate(&l.RG, validation.Required),
		"rg_orgao_expedidor":  validation.Validate(&l.Origin, validation.Required),
		"rg_date_of_emission": validation.Validate(&l.EmissionDate, validation.Required, validation.Match(regexp.MustCompile(`([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))`))),
		"uf_orgao_expedidor": validation.Validate(&l.EmissionState, validation.Required, validation.In("AC", "AL", "AP", "AM", "BA", "CE", "DF", "GO", "ES", "MA", "MT", "MS", "MG", "PA", "PB", "PR",
			"PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SP", "SC", "SE", "TO")),
		"cellphone_number":            validation.Validate(&l.Cellphone, validation.Required, validation.Match(regexp.MustCompile(`(\d{2})(9[1-9]\d{7})`))),
		"phone_number":                validation.Validate(&l.Phone, validation.Required, validation.Match(regexp.MustCompile(`(\d{2})(([2-8]\d{7})|9[1-9]\d{7})`))),
		"mother_name":                 validation.Validate(&l.MotherName, validation.Required),
		"marital_state":               validation.Validate(&l.MaritalState, validation.Required, validation.In("SOLTEIRO", "CASADO", "DIVORCIADO", "VIUVO", "UNIAO ESTAVEL")),
		"naturality":                  validation.Validate(&l.Naturality, validation.Required),
		"scholarity":                  validation.Validate(&l.Scholarity, validation.Required, validation.In("Fundamental", "Ensino Médio", "Superior Incompleto", "Superior Completo", "Pós-Graduado")),
		"employment":                  validation.Validate(&l.Employment, validation.Required),
		"employment_company":          validation.Validate(&l.Company, validation.Required),
		"employment_post":             validation.Validate(&l.JobTitle, validation.Required),
		"employment_salary":           validation.Validate(&l.Salary, validation.Required, validation.Match(regexp.MustCompile(`\d+.\d{2}`))),
		"employment_status":           validation.Validate(&l.Occupation, validation.Required, validation.In("Empregado Setor Privado", "Empregado Setor Publico", "Profissional Liberal", "Empresario", "Aposentado ou Pensionista", "Autonomo", "Outros")),
		"address_street":              validation.Validate(&l.Address, validation.Required),
		"address_number":              validation.Validate(&l.Number, validation.Required),
		"address_complement":          validation.Validate(&l.Complement, validation.Required),
		"address_neighborhood":        validation.Validate(&l.Neighborhood, validation.Required),
		"address_city":                validation.Validate(&l.City, validation.Required),
		"address_state":               validation.Validate(&l.State, validation.Required),
		"address_zip_code":            validation.Validate(&l.ZipCode, validation.Required),
		"address_residence_type":      validation.Validate(&l.ResidentType, validation.Required),
		"account_bank_code,omitempty": validation.Validate(&l.BankCode, validation.Required),
		"account_branch_number":       validation.Validate(&l.BranchNumber, validation.Required, validation.Match(regexp.MustCompile(`\d+[^-]+\d+`))),
		"account_number":              validation.Validate(&l.AccountNumber, validation.Required, validation.Match(regexp.MustCompile(`\d+[^-]+\d+`))),
		"loan_principal":              validation.Validate(&l.LoanPrincipal, validation.Required, validation.Match(regexp.MustCompile(`\d+.\d{2}`))),
		"loan_instalment_number":      validation.Validate(&l.LoanInstalmentNumber, validation.Required),
		"loan_reason":                 validation.Validate(&l.LoanReason, validation.Required),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}
