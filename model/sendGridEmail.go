package model

type Email struct {
	Subject      string `json:"subject,omitempty" bson:"subject,omitempty"`
	PlainContent string `json:"plainContent,omitempty" bson:"plainContent,omitempty"`
	HTMLContent  string `json:"htmlContent,omitempty" bson:"htmlContent,omitempty"`
}
