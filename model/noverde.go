package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"net/http"
	"regexp"
)

type NvrdLoan struct {
	LoanId string `json:"loan_id,omitempty" bson:"loan_id,omitempty"`
	Status string `json:"status,omitempty" bson:"status,omitempty"`
	URL    string `json:"url,omitempty" bson:"url,omitempty"`
}

type NvrdLoanRequest struct {
	Amount       float64 `json:"amount,omitempty" bson:"amount,omitempty"`
	Period       int64   `json:"period,omitempty" bson:"period,omitempty"`
	Payday       int64   `json:"payday,omitempty" bson:"payday,omitempty"`
	Name         string  `json:"name,omitempty" bson:"name,omitempty"`
	Email        string  `json:"email,omitempty" bson:"email,omitempty"`
	CPF          string  `json:"cpf,omitempty" bson:"cpf,omitempty"`                     // 14160094775
	MobileNumber string  `json:"mobile_number,omitempty" bson:"mobile_number,omitempty"` // 21994402827
	Income       float64 `json:"income,omitempty" bson:"income,omitempty"`
	Occupation   string  `json:"occupation,omitempty" bson:"occupation,omitempty"` //EMPREGADO, FUNCIONARIO_PUBLICO, MICRO_EMPREENDEDOR, TRABALHADOR_INFORMAL, APOSENTADO_PENSIONISTA, DESEMPREGADO
	Birthdate    string  `json:"birthdate,omitempty" bson:"birthdate,omitempty"`   // aaaa/mm/dd
}

func (l NvrdLoanRequest) Validate() error {
	err := validation.Errors{
		"amount":        validation.Validate(&l.Amount, validation.Required, validation.Min(1000.0), validation.Max(4000.0)),
		"period":        validation.Validate(&l.Period, validation.Required),
		"payday":        validation.Validate(&l.Payday, validation.Required),
		"name":          validation.Validate(&l.Name, validation.Required),
		"email":         validation.Validate(&l.Name, validation.Required),
		"cpf":           validation.Validate(&l.CPF, validation.Required, validation.Match(regexp.MustCompile(`\d{11}`))), // TODO Validar numero?
		"mobile_number": validation.Validate(&l.MobileNumber, validation.Required, validation.Match(regexp.MustCompile(`(\d{2})(9[1-9]\d{7})`))),
		"income":        validation.Validate(&l.Income, validation.Required, validation.Min(1000.0)),
		"occupation":    validation.Validate(&l.Occupation, validation.Required, validation.In("EMPREGADO", "FUNCIONARIO_PUBLICO", "MICRO_EMPREENDEDOR", "TRABALHADOR_INFORMAL", "APOSENTADO_PENSIONISTA", "DESEMPREGADO")),
		"birthdate":     validation.Validate(&l.Birthdate, validation.Required, validation.Match(regexp.MustCompile(`([12]\d{3}/(0[1-9]|1[0-2])/(0[1-9]|[12]\d|3[01]))`))),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		utils.LogInfo(errFields)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}
