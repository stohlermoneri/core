package model

import (
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"strconv"
	"time"
)

type ProposalOffer struct {
	UUID                 string  `json:"uuid,omitempty" bson:"uuid,omitempty"`
	PartnerName          string  `json:"partnerName" bson:"partnerName,omitempty"`
	LoanValue            float64 `json:"loanValue" bson:"loanValue,omitempty"`
	InstallmentValue     float64 `json:"installmentValue" bson:"installmentValue,omitempty"`
	NumberOfInstallments int64   `json:"numberOfInstallments" bson:"numberOfInstallments,omitempty"`
	MonthlyInterestRate  float64 `json:"monthlyInterestRate" bson:"monthlyInterestRate,omitempty"`
	AnnualInterestRate   float64 `json:"annualInterestRate" bson:"annualInterestRate,omitempty"`
	TotalTaxes           float64 `json:"totalTaxes" bson:"totalTaxes,omitempty"`
	LoanDate             string  `json:"loanDate" bson:"loanDate,omitempty"`
	LoanFinalDate        string  `json:"loanFinalDate" bson:"loanFinalDate,omitempty"`
	OfferDescription     string  `json:"offerDescription,omitempty" bson:"offerDescription,omitempty"`
	RedirectUrl          string  `json:"redirectUrl" bson:"redirectUrl,omitempty"`
}

type ProposalData struct {
	GeruOffer   GeruOffer   `json:"geruOffer,omitempty" bson:"geruOffer,omitempty"`
	GeruLoan    GeruLoan    `json:"geruLoan,omitempty" bson:"geruLoan,omitempty"`
	NvrdLoan    NvrdLoan    `json:"nvrdLoan,omitempty" bson:"nvrdLoan,omitempty"`
	RebelLoan   RebelLoan   `json:"rebelLoan,omitempty" bson:"rebelLoan,omitempty"`
	SimplicLoan SimplicLoan `json:"simplicLoan,omitempty" bson:"simplicLoan,omitempty"`
	SimLoan     SimLoan     `json:"simLoan,omitempty" bson:"simLoan,omitempty"`
}

type ProposalResponse struct {
	Id              primitive.ObjectID `json:"id,omitempty" bson:"id,omitempty"`
	UtmSource       string             `json:"utm_source" bson:"utm_source"`
	ProposalStatus  string             `json:"proposalStatus,omitempty" bson:"proposalStatus,omitempty"`
	SelectedPartner string             `json:"selectedPartner,omitempty" bson:"selectedPartner,omitempty"`
	ProposalOffers  []ProposalOffer    `json:"proposalOffers" bson:"proposalOffers"`
	ProposalData    ProposalData       `json:"proposalData,omitempty" bson:"proposalData,omitempty"`
	CreatedAt       time.Time          `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	UpdatedAt       time.Time          `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
}

func SimToProposalOffers(offers []SimOffer) []ProposalOffer {
	var proposalOffers []ProposalOffer

	for _, simOffer := range offers {
		offer := ProposalOffer{
			UUID:                 simOffer.UUID,
			PartnerName:          "SIM",
			LoanValue:            simOffer.TotalFinancedValue,
			InstallmentValue:     simOffer.InstallmentValue,
			NumberOfInstallments: simOffer.InstallmentAmount,
			MonthlyInterestRate:  simOffer.CETMensal,
			AnnualInterestRate:   simOffer.CETAnual,
			TotalTaxes:           0,
			LoanDate:             utils.DashToSlashDate(simOffer.FirstInstallmentDate),
			OfferDescription:     simOffer.OfferDescription,
			RedirectUrl:          "", //TODO Vazio mesmo?
		}

		proposalOffers = append(proposalOffers, offer)
	}
	return proposalOffers
}

func GeruToProposalOffers(result *Result, proposal ProposalResponse) []ProposalOffer {

	var proposalOffers []ProposalOffer

	for _, offer := range result.Offers {

		loanDate, loanFinalDate := utils.DateToPeriod(proposal.CreatedAt, offer.InstalmentNumber)

		geruOffer := ProposalOffer{
			PartnerName:          "GERU",
			LoanValue:            offer.Principal,
			InstallmentValue:     offer.InstalmentValue,
			NumberOfInstallments: offer.InstalmentNumber,
			MonthlyInterestRate:  offer.InterestRate,
			LoanDate:             loanDate,
			LoanFinalDate:        loanFinalDate,
		}

		if proposal.ProposalData.GeruLoan.URL != "" {
			geruOffer.RedirectUrl = proposal.ProposalData.GeruLoan.URL
		} else if proposal.ProposalData.GeruOffer.URL != "" {
			geruOffer.RedirectUrl = proposal.ProposalData.GeruOffer.URL
		}

		proposalOffers = append(proposalOffers, geruOffer)
	}

	return proposalOffers
}

func RebelToProposalOffers(proposal ProposalResponse) []ProposalOffer {

	var proposalOffers []ProposalOffer

	for _, suggestion := range proposal.ProposalData.RebelLoan.Suggestions {

		date, _ := time.Parse("2006-01-02", suggestion.LoanDate)
		loanDate, loanFinalDate := utils.DateToPeriod(date, suggestion.NumberOfInstallments)

		rebelOffer := ProposalOffer{
			PartnerName:          "REBEL",
			LoanValue:            float64(suggestion.LoanValue),
			InstallmentValue:     suggestion.InstallmentValue,
			NumberOfInstallments: suggestion.NumberOfInstallments,
			MonthlyInterestRate:  suggestion.MonthlyInterestRate,
			LoanDate:             loanDate,
			LoanFinalDate:        loanFinalDate,
			RedirectUrl:          proposal.ProposalData.RebelLoan.URL + "&prazo=" + strconv.Itoa(int(suggestion.NumberOfInstallments)),
		}

		proposalOffers = append(proposalOffers, rebelOffer)
	}

	return proposalOffers
}

func SimplicToProposalOffers(offers []SimplicOffer, proposal ProposalResponse) []ProposalOffer {

	var proposalOffers []ProposalOffer

	for _, offer := range offers {

		loanValue, err := strconv.ParseFloat(offer.Amount, 64)
		if err != nil {
			utils.LogError(err.Error())
			continue
		}
		numOfInstallments, err := strconv.Atoi(offer.NumInstallments)
		if err != nil {
			utils.LogError(err.Error())
			continue
		}
		interest, err := strconv.ParseFloat(offer.InterestRate, 64)
		if err != nil {
			utils.LogError(err.Error())
			continue
		}
		installmentValue, err := strconv.ParseFloat(offer.InstallmentAmount, 64)
		if err != nil {
			utils.LogError(err.Error())
			continue
		}
		loanDate, loanFinalDate := utils.DateToPeriod(proposal.CreatedAt, int64(numOfInstallments))

		po := ProposalOffer{
			PartnerName:          "SIMPLIC",
			LoanValue:            loanValue,
			InstallmentValue:     installmentValue,
			NumberOfInstallments: int64(numOfInstallments),
			MonthlyInterestRate:  interest * 100,
			LoanDate:             loanDate,
			LoanFinalDate:        loanFinalDate,
			RedirectUrl:          proposal.ProposalData.SimplicLoan.ConfirmationURL,
		}

		proposalOffers = append(proposalOffers, po)
	}

	return proposalOffers
}
