package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"bitbucket.org/guilhermesilvafluxx/core/fxerror"
	"bitbucket.org/guilhermesilvafluxx/core/utils"
	"net/http"
	"time"
)

type Update struct {
	UpdatedAt        time.Time `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
	NotificationText string    `json:"notificationText,omitempty" bson:"notificationText,omitempty"`
	CreatedBy        string    `json:"createdBy,omitempty" bson:"createdBy,omitempty"` // UserId from editor
}

type Notification struct {
	Id                  string    `json:"id,omitempty" bson:"id,omitempty"`
	Status              string    `json:"status,omitempty" bson:"status,omitempty"` // ACTIVE, INACTIVE, DELETED
	MerchantId          []string  `json:"merchantId,omitempty" bson:"merchantId,omitempty"`
	Delay               int64     `json:"delay" bson:"delay"`
	NotificationType    string    `json:"notificationType,omitempty" bson:"notificationType,omitempty"`       // SMS, Email
	NotificationContext string    `json:"notificationContext,omitempty" bson:"notificationContext,omitempty"` // RECOVERY_SIGNIN, RECOVERY_PARTNER, CONTRACT_CONFIRM
	NotificationRetry   int64     `json:"notificationRetry,omitempty" bson:"notificationRetry,omitempty"`     // 1 to 3
	NotificationText    string    `json:"notificationText,omitempty" bson:"notificationText,omitempty"`
	NotificationHTML    string    `json:"notificationHTML,omitempty" bson:"notificationHTML,omitempty"`
	NotificationSubject string    `json:"notificationSubject,omitempty" bson:"notificationSubject,omitempty"`
	CreatedAt           time.Time `json:"createdAt,omitempty" bson:"createdAt,omitempty"`
	CreatedBy           string    `json:"createdBy,omitempty" bson:"createdBy,omitempty"` // UserId from creator
	UpdatedAt           time.Time `json:"updatedAt,omitempty" bson:"updatedAt,omitempty"`
	UpdatedHistory      []Update  `json:"updatedHistory,omitempty" bson:"updatedHistory,omitempty"`
}

func (n Notification) Validate() error {
	err := validation.Errors{
		"status":              validation.Validate(&n.Status, validation.Required, validation.In("ACTIVE", "INACTIVE", "DELETED")),
		"merchantId":          validation.Validate(&n.MerchantId, validation.Required),
		"notificationType":    validation.Validate(&n.NotificationType, validation.Required, validation.In("sms", "email")),
		"notificationContext": validation.Validate(&n.NotificationContext, validation.Required, validation.In("RECOVERY_SIGNIN", "RECOVERY_PARTNER", "CONTRACT_CONFIRM", "CONFIRMED", "DENIED")),
		"notificationRetry":   validation.Validate(&n.NotificationRetry, validation.Required, validation.Min(1), validation.Max(3)),
		"notificationText":    validation.Validate(&n.NotificationText, validation.Required),
		"createdBy":           validation.Validate(&n.CreatedBy, validation.Required),
	}.Filter()

	// Gets fields that have error
	var errFields string
	if err != nil {
		errFields = utils.GetErrorFields(err)
		return fxerror.NewError("invalid-field", fxerror.InvalidField, errFields, "", http.StatusBadRequest, fxerror.InvalidRequest, err)
	}

	return nil
}
